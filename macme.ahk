﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

;Goto end of line (win+right):
#Right::
	Send, {End}
Return

;Goto start of line (win+left):
#Left::
	Send, {Home}
Return

;skip a word left:
!Left::
	Send, {Ctrl down}{Left}{Ctrl up}
Return

;skip a word right:
!Right::
	Send, {Ctrl down}{Right}{Ctrl up}
Return

;Page up
#Up::
	Send, {PgUp}
Return

;Page down
#Down::
	Send, {PgDn}
Return

;Not too sure?
!3::
	Send, +3
Return

;Select to start of line
#+Left::
	Send, +{Home}
Return

;Select to end of line
#+Right::
	Send, +{End}
Return

;Prevent the win+enter key from opening accessibility tools
#Enter:: Send, {Enter}

;some VS specific commands
#IfWinActive ahk_exe devenv.exe

;Move line down (resharper only)
;#Down::
;	Send, ^+!{Down}
;Return 

;Move line up (resharper only)
;#Up::
;	Send, ^+!{Up}
;Return 

;Refactor (resharper only)
;#r::
;	Send, {F2}
;Return

;Refactor (vs)
#r::
	Send, {Ctrl down}rr{Ctrl up}
Return

;Find usages
+#r::
	Send, +{F12}
Return

;Goto definition
#d::
	Send, {F12}
Return

;Auto format document (win+shift+w)
#+w::
	Send, {Ctrl down}kd{Ctrl up}
Return

;Prevent win+shift+down from minimizing window
#+Down::
	Send, +{Down}
Return

;Comment line
#/::
	Send, {Ctrl down}kc{Ctrl up}
Return

;Un-Comment line
#+/::
	Send, {Ctrl down}ku{Ctrl up}
Return

;Find classes / symbols
^n::
	Send, {Ctrl down}{,}{Ctrl up}
Return

#IfWinActive

;copy and paste for gitbash
#IfWinActive ahk_exe mintty.exe

^c::
	Send, ^{Ins}
Return

^v::
	Send, +{Ins}
	
;#^c::
;	Send, ^c
;Return

Return

#IfWinActive