# Autohotkey mappings

1. download Autohotkey from https://autohotkey.com
2. clone this repo
3. run macme.ahk (and add a shortcut to your start-up folder)

### General purpose
- CMD+LEFT: Move cursor to start of line
- CMD+RIGHT: Move cursor to end of line
- ALT+LEFT: Skip word left
- ALT+RIGHT: Skip word right
- CMD+UP: Page up
- CMD+DOWN: Page down
- CMD+SHIFT+LEFT: Select all text from cursor to start of line
- CMD+SHIFT+RIGHT: Select all text from cursor to end of line

### Visual Studio Specific
- CMD+R: Refactor
- CMD+SHIFT+R: Find all usages
- CMD+SHIFT+W: Autoformat document
- CMD+/: Comment current line / selection
- CMD+SHIFT+/: Un-comment current line / selection - Note: if using parallels you will need to configure the cmd+shift+/ combo to be passed on to the guest os.
- ALT+DOWN: Move line down
- ALT+UP: Move line up

### MINGW (GitBash)
- CMD+C: Copy
- CMD+V: Paste
- CTRL+ALT+C: Send interrupt signal